const bcrypt = require('bcryptjs');
// const jwt = require('jsonwebtoken');
require('dotenv').config()

const { User } = require('../models/Users');

const changeProfilePassword = async (req, res) => {
    try {
        if (typeof req.body.newPassword === 'undefined' || req.body.newPassword.trim === '') {
            return res.status(400)
                .json({ message: 'Password is empty' });
        }
        const { newPassword } = req.body;
        const cryptPassword = bcrypt.hashSync(newPassword, 10);
        const oldPassword = await User.findById({
            _id: req.user.userId,
        })

      const user =  await User.findByIdAndUpdate(
            { _id: req.user.userId },
            { $set: { password: cryptPassword } },
        )
                if (!user) {
                    return res.status(400)
                        .json({ message: "User isn't found" });
                }

        return res.status(200).json({
                     message: "Success"
                })
    } catch (e) {
        return res.status(500)
            .json({ message: 'Server error' });
    }
};


const deleteProfile = (req, res) => {
  try {
    return User.findByIdAndDelete({
      _id: req.user.userId,
    })
      .then((user) => {
        if (!user) {
          return res.status(400)
            .json({ message: "User isn't found" });
        }
        return res.status(200)
          .json({ message: 'Success', ...user });
      });

  } catch (e) {
    return res.status(500)
      .json({ message: 'Server error' });
  }
};

const getProfileInfo = (req, res) => {
  try {
    return User.findById({
      _id: req.user.userId,
    })
      .then((user) => {

          if (!user || typeof user ==='null') {
          return res.status(400)
            .json({ message: "User isn't found" });
        }
        const {_id,
            username,
            createdDate} = user._doc
        return res.status(200)
          .json({"user":{
              _id,username,createdDate
              }});
      });
  } catch (e) {
    return res.status(500)
      .json({ message: 'Server error' });
  }
};
module.exports = {
  changeProfilePassword,
  deleteProfile,
  getProfileInfo,
};
