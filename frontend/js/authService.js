import {setQuery} from "./helper/helper.js";

async function authLogin(body) {
      return    await setQuery('/api/auth/login', 'post', body)
}

async function authRegister(body) {
    return await setQuery('/api/auth/register', 'post', body)
}
async function getProfileServ() {
     return await setQuery('/api/users/me', 'get')
}
async function saveProfileServ(body) {
    return await setQuery('/api/users/me', 'patch', body)
}
async function deleteProfileServ() {
    return await setQuery('/api/users/me', 'delete')
}

export {authLogin, authRegister,getProfileServ,saveProfileServ,deleteProfileServ}
