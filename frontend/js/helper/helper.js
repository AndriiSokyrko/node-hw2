// const axios = require('axios');
import axios from 'axios'
const store = window.localStorage
// store.clear();
async function setQuery(url , method = 'get', body = null, headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods' : 'POST,GET,OPTIONS,PUT,DELETE',
    'Authorization': 'Bearer '+ localStorage.jwt_token
}) {
    const path ='http://127.0.0.1:8080'
    setLoading(true);
    try {
        let options ={
            url:url,
            baseURL:path,
            headers:headers,
            responseType:'json'
        }
        let data = JSON.stringify(body)
        let res =''

        if(method==='post') {
            res = await axios.post(url, data, options)
        }
        if(method==='put') {

            res = await axios.put(url, data, options)

        }
        if(method==='patch') {
            res =  await axios.patch(path+url,data,options)
        }
        if(method==='get' ) {
            res =  await axios.get(url,options)
        }

        if( method==='delete'  ) {
            res = await axios.delete(url, options)
        }

           return res
    } catch (e) {
        setLoading(false);
        setError(e);
        return  Error('Server error 400')
    }
}
function setLoading(status) {
    return  status
}

function setError(status) {
    return status
}

async function login( body) {
    try {
        const response = await setQuery('/api/auth/login', 'post', body)
            const {jwt_token} = response.data
            store.jwt_token = jwt_token
            loginedUser.classList.remove('hide')
            formAuth.classList.remove('formAuth')
            formAuth.classList.add('hide')
    } catch(e){
        return 'User not found'
    }

}
function message(message){
    message_alert.innerHTML = '<h3>'+message+' </h3>'
    message_alert.classList.add('block--message')
    setTimeout(() => {
        message_alert.classList.add('hide')
        message_alert.classList.remove('block--message')

    }, 1000)
}

async function  getNotes() {
    try {
        const responce = await setQuery('/api/notes')
        return  responce.data.notes
    } catch (e) {
    }
    return 'No notes'
}

    async function  deleteNote(_id) {
       let res  = await setQuery('/api/notes/' + _id, 'delete')
        message('Note was deleted')
        return res

    }
export {setQuery, setLoading, setError, login,  deleteNote, message,getNotes}
