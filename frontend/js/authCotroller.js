import   {authLogin, authRegister,getProfileServ,
    saveProfileServ,deleteProfileServ} from '../js/authService.js'
const store = window.localStorage

async function register(body){
    const res = await authRegister(body)
    console.log(res);
    res.then(response => {
        if (response.status === 400) {
            return response.status(400).json({"message": "User is already exist"})
        }
        if (response.status === 200) {
            login( body)
        }

    }).catch(e => {
        if (e.response.status === 400) {
            message_alert.innerHTML = '<h3>Пользователь с таким именем зарегестрирован</h3>'
            message_alert.classList.add('block--message')
            setTimeout(() => {
                message_alert.classList.add('hide')
                message_alert.classList.remove('block--message')

            }, 1000)
            return
        }
        return e.status(500)
    })
}
async function login( body) {
    let res = await authLogin(body)

    if (res.status == 200) {
        let {jwt_token} = res.data
        store.setItem("jwt_token" , jwt_token)
        return
    }
    store.setItem("jwt_token" , '')
    window.location.href='/'
}
async function getPofile(){
    return await getProfileServ()
}
async function savePofile(body){
    return await saveProfileServ(body)
}
async function delPofile(){
    return await deleteProfileServ()
}

export {register, login, getPofile, savePofile, delPofile}
