import {setQuery} from "./helper/helper.js";

async function getAllNotes() {
         return  await setQuery('/api/notes')
    }
 function    getNoteById(id) {
        return setQuery('/api/notes/' + id)

    }
 async function createNewNote({text,completed}) {
        return  await setQuery('/api/notes', 'post', {text,completed})

    }

 function   complitedNoteById(id) {
        return setQuery('/api/notes/'+id, 'patch',{id})
    }



 async function    modiNoteById(id, payload) {
        return await setQuery('/api/notes/' + id, 'put', payload)

}
export {modiNoteById,createNewNote,complitedNoteById,getAllNotes,getNoteById}
