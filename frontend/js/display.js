import {deleteNote, getNotes} from "./helper/helper";
async function notes(hideForm){
    const blockNotes = document.querySelector('#block_notes')
    blockNotes.innerHTML=''
    blockNotes.classList.remove('hide')
    blockNotes.innerHTML =''
    const dates =  await getNotes(card)
    let blockCards = document.createElement('div')
    blockCards.classList.add('block--notes')
    if(!dates.length){
        blockNotes.innerHTML='<h3>No dates</h3>'
        return
    }
    dates.map(elm=>{
        let templ = card(elm, hideForm)
        blockCards.append(templ)
    })
    blockNotes.append(blockCards)
}
function card(payload,hideForm){
    const changeNotesForm = document.querySelector('#change_notes_form')
    let wrap = document.createElement('div')
    wrap.classList.add('card')
    wrap.setAttribute('id',payload._id)
    let h5 = document.createElement('h5')
    h5.classList.add('card-title')
    let p1 = document.createElement('p')
    p1.classList.add('card-text')
    let p2 = document.createElement('p')
    p2.classList.add('card-text')
    let p3 = document.createElement('p')
    p3.classList.add('card-text')

    let link = document.createElement('a')
    link.classList.add('btn-primary')
    link.classList.add('btn')
    link.innerText='More'
    h5.innerText=payload._id
    wrap.append(h5)
    p1.innerText= payload.text
    wrap.append(p2)
    p2.innerText=payload.userId
    wrap.append(p1)
    p3.innerText=payload.completed
    wrap.append(p3)
    let linkWrap = document.createElement('div')
    linkWrap.classList.add('link--wrap')
    link.classList.add('cardMore')
    link.setAttribute('id',payload._id)
    link.addEventListener('click',(e) => {
        e.preventDefault()
        hideForm()
        changeNotesForm.classList.remove('hide')
        noteTextMody.value= payload.text
        idNote.value=payload._id
        userId.value = payload.userId

    })
    linkWrap.append(link)

    let link1 = document.createElement('a')
    link1.classList.add('btn-danger')
    link1.classList.add('ms-2')
    link1.classList.add('btn')
    link1.innerText='Delete'
    link1.classList.add('deleteNote')
    link1.addEventListener('click',async (e) => {

            deleteNote(payload._id)
            setTimeout(()=>{

                notes(card,hideForm)
            },100)


    })
    linkWrap.append(link1)
    wrap.append(linkWrap)

    return wrap
}

 export  {card, notes}
