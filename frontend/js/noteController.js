import {modiNoteById,createNewNote,complitedNoteById,getAllNotes,getNoteById} from '../js/noteService.js'

async function  getNotesController() {
    try {
        const responce = getAllNotes()
        return  responce.data.notes
    } catch (e) {
    }
    return 'No notes'
}
async function  createNoteController(payload) {
    try {
        const responce = createNewNote(payload)
        return  responce.data.notes
    } catch (e) {
        return 'No notes'
    }

}
async function  completedNoteController(id) {

    try{
        await complitedNoteById(id)
    } catch (e){
        return 'Error 404'
    }
}
async function  modifyNoteController(id,{text}) {

    try{
        await modiNoteById(id,{text})
    } catch (e){
        return 'Error 404'
    }
}
async function  getNoteByIdController(id) {

    try{
        await getNoteById(id)
    } catch (e){
        return 'Error 404'
    }
}
export {getNotesController, createNoteController, completedNoteController,modifyNoteController,getNoteByIdController}
