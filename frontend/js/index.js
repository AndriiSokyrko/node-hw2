import('../scss/styles.scss')
import {setQuery, message, getNotes} from '../js/helper/helper.js'
import  {register,login, getPofile,savePofile,delPofile} from '../js/authCotroller.js'
import {
    getNotesController,
    createNoteController,
    completedNoteController,
    modifyNoteController
} from '../js/noteController.js'
import {card, notes} from '../js/display.js'
let checkLog = false
const store = window.localStorage
const loginedUser = document.querySelector('#loginedUser')
const btnLoginAuth = document.querySelector('#btnLoginAuth')
const checkBtn = document.querySelector('#check')
const formAuth = document.querySelector('#formAuth')
const password = document.querySelector('#password')
const username = document.querySelector('#name')
const linkSignOut = document.querySelector('#link_sign_out')
const blockNotes = document.querySelector('#block_notes')
const linkNotes = document.querySelector('#link_notes')
let checkNewNote = false
const linkAddNote = document.querySelector('#link_add_note')
const changeNotesForm = document.querySelector('#change_notes_form')
const btnSaveNewNote = document.querySelector('#saveNewNote')
const addNoteForm = document.querySelector('#add_note_form')
const btnChangeNoteBtn = document.querySelector('#btnChangeNote')
const noteTextMody = document.querySelector('#change_notes_form #noteTextMody')
const chkbox = document.querySelector('#change_notes_form #modyCompleteCkbox')
const userId = document.querySelector('#change_notes_form #userId')
const linkDeleteProfile = document.querySelector('#link_delete_profile')

addNoteForm.classList.add('hide')

const messageAlert = document.querySelector('#message_alert')
let checked = false
function hideMenu(){
    if (store.getItem("jwt_token") == '') {
        loginedUser.classList.add('hide')
        formAuth.classList.add('formAuth')
        formAuth.classList.remove('hide')

    }
}
 hideMenu()
if (store.getItem("jwt_token")!=='') {
    // hideForm()
    loginedUser.classList.remove('hide')
    formAuth.classList.remove('formAuth')
    formAuth.classList.add('hide')

}

function hideForm() {
    addNoteForm.classList.add('hide')
    getProfileForm.classList.add('hide')
    blockNotes.classList.add('hide')
    changeNotesForm.classList.add('hide')
    // loginedUser.classList.add('hide')
    formAuth.classList.add('hide')

}
 // hideForm()
checkBtn.addEventListener('click', (e) => {
    checked = !checked
    checkBtn.setAttribute('checked', checked)
    btnLoginAuth.innerHTML = checked ? 'Register' : 'Login'
})

linkSignOut.addEventListener('click', (e) => {
    e.preventDefault()
    store.setItem('jwt_token','')
    hideForm()
    loginedUser.classList.add('hide')
    formAuth.classList.add('formAuth')

})
//register

btnLoginAuth.addEventListener('click', async (e) => {
    e.preventDefault()
    let body = {
        username: username.value,
        password: password.value
    }
    if (!checked) {
            await  login(body)
            if(store.getItem('jwt_token')===''){
                loginedUser.classList.add('hide')
                messageAlert.innerHTML = "<h3>Login or pasword isn't corret</h3>"
                setTimeout(() => {
                    messageAlert.classList.add('hide')
                    messageAlert.classList.remove('block--message')
                },1000)
            return
            }
            loginedUser.classList.remove('hide')

            formAuth.classList.remove('formAuth')
            formAuth.classList.add('hide')

    } else {
        register(body)

    }
})

//all notes

  linkNotes.addEventListener('click', async (e) => {
    e.preventDefault()
    hideForm()
     notes(hideForm)

    // blockCards.then(block=>{
    //     blockNotes.append(block )
    //
    // })
      // blockNotes.append(blockCards)
      // getNotesController(card,hideForm)
})


//add new note

linkAddNote.addEventListener('click', (e) => {
    e.preventDefault()
    hideForm()
    addNoteForm.classList.remove('hide')
    checkNewNote = !checkNewNote

})
//NEW NOTE
btnSaveNewNote.addEventListener('click', async (e) => {
    e.preventDefault()
    const text = document.querySelector('#add_note_form #text').value
    const check = document.querySelector('#add_note_form #check')
    let completed = check.checked ? true : false;
    const res = createNoteController({text,completed})
    if (res) {
        messageAlert.innerHTML = '<h3>Note is added</h3>'
        messageAlert.classList.add('block--message')

        setTimeout(() => {
            messageAlert.classList.add('hide')
            messageAlert.classList.remove('block--message')

        }, 1000)
        addNoteForm.classList.add('hide')
        return res
    } else {
        return  'error 400';
    }

})
//CHANGE NOTE
// const addNoteForm = document.querySelector('#add_note_form')


btnChangeNoteBtn.addEventListener('click', async (e) => {
    e.preventDefault()
    hideForm()
    const text = document.querySelector('#change_notes_form #noteTextMody')
    const check = document.querySelector('#change_notes_form #modyCompleteCkbox')
    const id = document.querySelector('#change_notes_form #idNote').value

    if(check.checked){
        completedNoteController(id)
    }
    const res = modifyNoteController(id, {text: text.value})
    if (!res) {
        return 'error 400';
    }
    messageAlert.innerHTML = '<h3>Note is changed</h3>'
    messageAlert.classList.add('block--message')
    setTimeout(() => {
        messageAlert.classList.add('hide')
        messageAlert.classList.remove('block--message')

    }, 1000)

    return res


})

///search note

const searchById = document.querySelector('#searchById')
const searchValueId = document.querySelector('#searchValueId')
searchById.addEventListener('click', async (e) => {
    e.preventDefault()
    hideForm()

    const res = await setQuery('/api/notes/' + searchValueId.value)
    noteTextMody.value = res.data.note.text
    idNote.value = res.data.note._id
    userId.value = res.data.note.userId
    changeNotesForm.classList.remove('hide')
    if (!res.data.note.completed) {
        chkbox.checked = false
    }
    return
})


const linkGetProfile = document.querySelector('#link_get_profile')
const getProfileForm = document.querySelector('#get_profile_form')
const getProfileFormUserId = document.querySelector('#get_profile_form #userId')
const getProfileFormUserName = document.querySelector('#get_profile_form #userName')
const getProfileFormUserData = document.querySelector('#get_profile_form #dataCreated')
const getProfileFormPassword = document.querySelector('#get_profile_form #userPassword')
const btnSaveProfile = document.querySelector('#get_profile_form #btnSaveProfile')

linkGetProfile.addEventListener('click', async (e) => {
    e.preventDefault()
    hideForm()
    getProfileForm.classList.remove('hide')
     let res= await getPofile()
    getProfileFormUserName.value = res.data.user.username
    getProfileFormUserId.value = res.data.user._id
    getProfileFormUserData.value = res.data.user.createdDate

    return
})
btnSaveProfile.addEventListener('click', (e) => {
    e.preventDefault()
    let body = {
        id:getProfileFormUserId.value,
        username: getProfileFormUserName.value,
        newPassword: getProfileFormPassword.value
    }
    const res =savePofile(body)
    res.then(response => {
        if (response.status === 400) {
            return response.status(400).json({"message": "User is already exist"})
        }
        if (response.status === 200) {
            messageAlert.innerHTML = '<h3>Password is changed</h3>'
            messageAlert.classList.add('block--message')
            setTimeout(() => {
                messagAlertlert.classList.add('hide')
                messagAlertlert.classList.remove('block--message')

            }, 1000)

            // login()
            getProfileForm.classList.add('hide')
        }
    })
})

linkDeleteProfile.addEventListener('click', async (e) => {
    e.preventDefault()
    getProfileForm.classList.remove('hide')
    const res =delPofile()
    e.preventDefault()
    store.jwt_token = ''

    formAuth.classList.add('formAuth')
    formAuth.classList.remove('hide')
    getProfileForm.classList.add('hide')
    loginedUser.classList.add('hide')


})
