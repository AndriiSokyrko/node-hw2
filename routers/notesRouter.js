const express = require('express');

const notesRouter = express.Router();

const {
  addUserNotes,
  getUserNotes,
  getNoteById,
  deleteUserNoteById,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
} = require('../controllers/notesService.js');
const { authMiddleware } = require('../middleware/authMiddleware.js');
notesRouter.post('/', authMiddleware, addUserNotes);

notesRouter.get('/', authMiddleware, getUserNotes);

notesRouter.get('/:id', authMiddleware, getNoteById);

notesRouter.put('/:id', authMiddleware, updateUserNoteById);

notesRouter.patch('/:id', authMiddleware, toggleCompletedForUserNoteById);

notesRouter.delete('/:id', authMiddleware, deleteUserNoteById);

module.exports = {
  notesRouter
};
